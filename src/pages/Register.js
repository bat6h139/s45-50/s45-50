import {useState, useEffect} from 'react';
import {Container, Form, Button} from 'react-bootstrap';

export default function Register(){

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [cpw, setCpw] = useState("")

	const [isDisabled, setIsDisabled] = useState(true)

	/*mini activity
		
		create a state for the button
			if email, pw, cpw is empty && pw does not match cpw, keep button disabled
			if states passed the requirement, activiate the button to be able to submit the form

			screenshot the state for the button & the useEffect codes
			answer by 6:20
	*/

	useEffect( ()=>{
		if((email !== "" && password !== "" && cpw !== "") && 
			(password === cpw)){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}

	}, [email, password, cpw])


	function Register(e){
		e.preventDefault()

		setEmail("")
		setPassword("")
		setCpw("")


		alert("Registered Successfully!")
	}

	return(

		<Container>
			<Form 
				className="border p-3 mb-3"
				onSubmit={ (e) => Register(e) }
				>
			{/*email*/}
			  <Form.Group className="mb-3" controlId="email">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value={email} 
			    	onChange={ (e) => setEmail(e.target.value) }
			    	/>
			  </Form.Group>
			{/*password*/}
			  <Form.Group className="mb-3" controlId="password">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password} 
			    	onChange={ (e) => setPassword(e.target.value) }
			    	/>
			  </Form.Group>
			{/*confirm password*/}
			<Form.Group className="mb-3" controlId="cpw">
			  <Form.Label>Verify Password</Form.Label>
			  <Form.Control 
			  		type="password" 
			  		placeholder="Verify Password" 
			  		value={cpw} 
			  		onChange={ (e) => setCpw(e.target.value) }
			  		/>
			</Form.Group>
			  
			  <Button 
			  		variant="primary" 
			  		type="submit"
			  		disabled={isDisabled}
			  		>
			    Submit
			  </Button>
			</Form>
		</Container>
	)
}